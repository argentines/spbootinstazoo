package es.kuguk.instazoo.repository;

import es.kuguk.instazoo.entity.Comment;
import es.kuguk.instazoo.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findAllByPost(Post post);

    Comment findByIdAndUserId(Long commentId, Long userId);
}
