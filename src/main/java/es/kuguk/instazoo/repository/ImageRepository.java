package es.kuguk.instazoo.repository;

import es.kuguk.instazoo.entity.ImageModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ImageRepository extends JpaRepository<ImageModel, Long>{

    Optional<ImageModel> findAllById(Long id);

    Optional<ImageModel> findByPostId(Long id);

    Optional<ImageModel> findByUserId(Long id);
}
